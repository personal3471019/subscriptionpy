from app import db
import random
import string

class User(db.Model):
    email = db.Column(db.String(120), primary_key=True)
    otp = db.Column(db.String(6))
    subscriptions = db.relationship('Subscription', backref='user', lazy=True)

    def __repr__(self):
        return '<User %r>' % self.email

class Subscription(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    service = db.Column(db.String(50), nullable=False)
    user_email = db.Column(db.String(120), db.ForeignKey('user.email'), nullable=False)

    def __repr__(self):
        return '<Subscription %r>' % self.service

def generate_otp():
    otp = ''.join(random.choices(string.digits, k=6))
    return otp

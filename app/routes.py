from flask import Blueprint, request, jsonify
from app.models import db, User, Subscription, generate_otp
import sendgrid
import os
import ast


bp = Blueprint('routes', __name__)

# Configure SendGrid
sg = sendgrid.SendGridAPIClient(api_key=os.environ.get('SENDGRID_API_KEY'))

# Define routes
@bp.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    email = data.get('email')

    # Check if email is already registered
    existing_user = User.query.filter_by(email=email).first()
    if existing_user:
        return jsonify({'error': 'Email already registered'}), 400

    # Generate OTP
    otp = generate_otp()

    # Save OTP for the user
    new_user = User(email=email, otp=otp)
    db.session.add(new_user)
    db.session.commit()

    # Send OTP to the user's email
    message = sendgrid.Mail(
        from_email='srijan.rajbamshi@gmail.com',
        to_emails=email,
        subject='Your OTP for Registration',
        html_content=f'Your OTP is: {otp}'
    )
    try:
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(str(e))
        return jsonify({'error': 'Failed to send OTP'}), 500

    return jsonify({'message': 'OTP sent successfully'}), 200

@bp.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    email = data.get('email')
    otp_entered = data.get('otp')

    # Check if email is registered
    user = User.query.filter_by(email=email).first()
    if not user:
        return jsonify({'error': 'Email not registered'}), 400

    # Check if OTP matches
    if otp_entered != user.otp:
        return jsonify({'error': 'Invalid OTP'}), 400

    # If OTP matches, consider user as authenticated
    # You can perform further actions like generating authentication tokens, etc.

    return jsonify({'message': 'Login successful'}), 200

@bp.route('/subscribe', methods=['POST'])
def subscribe():
    data = request.get_json()
    email = data.get('email')
    services = data.get('services')
    print(type(services))
    services=ast.literal_eval(services)

    # Check if email is registered
    user = User.query.filter_by(email=email).first()
    if not user:
        return jsonify({'error': 'Email not registered'}), 400

    # Check if services is a list
    if not isinstance(services, list):
        return jsonify({'error': 'Services must be provided as a list'}), 400

    # Check if all services are valid
    valid_services = ['Netflix', 'Spotify', 'Amazon Prime']
    for service in services:
        if service not in valid_services:
            return jsonify({'error': f'Invalid service: {service}'}), 400

    # Subscribe user to the selected services
    for service in services:
        if not Subscription.query.filter_by(user_email=email, service=service).first():
            subscription = Subscription(service=service, user_email=email)
            db.session.add(subscription)

    db.session.commit()

    return jsonify({'message': 'Subscriptions updated successfully'}), 200

@bp.route('/usage', methods=['GET'])
def usage():
    # Retrieve users and their subscriptions
    users_subscriptions = []
    users = User.query.all()
    for user in users:
        user_data = {
            'email': user.email,
            'subscriptions': [subscription.service for subscription in user.subscriptions]
        }
        users_subscriptions.append(user_data)

    return jsonify({'users': users_subscriptions}), 200
